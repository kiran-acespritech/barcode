# -*- coding: utf-8 -*-
##############################################################################
#
#    This module uses OpenERP, Open Source Management Solution Framework.
#    Copyright (C) 2015-Today BrowseInfo (<http://www.browseinfo.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
# 
from openerp import models, fields, api, _
from openerp.exceptions import Warning

class custom_product_barcode(models.TransientModel):
    _name = "product.barcodes"
    barcode = fields.Text("Enter Barcode Here")

    @api.multi
    def chk_barcode_availability(self, barcode):
        lot_obj = self.env['stock.production.lot']
        res=lot_obj.browse(lot_obj.search([('name','=',barcode)]))
        print "=================res",res
        return res

    @api.multi
    def go(self):
        data = str(self.barcode).split("\n")
        ids1 = self._context.get('active_id')
        trans_det_obj = self.env['stock.transfer_details_items']
        lot_obj = self.env['stock.production.lot']
        trans_det_obj_dummy = self.env['stock.transfer_details']
        ids = self.browse(self._ids)
        brw_id_trans_det = trans_det_obj.browse(ids1)
        trans_det_obj_dummy_brw = trans_det_obj_dummy.browse(self._context.get('active_ids'))
        value = divmod(brw_id_trans_det.quantity,((len(data))-1))
        print "===================value",value
        remainder=value[1]
        create_value=[]
        lot_ids=[]
        if (brw_id_trans_det.quantity == ((len(data))-1)):
            counter = 1
            for i in data:
                if i!="":
                    barcode_brw = self.chk_barcode_availability(i)
                    if counter == 1:
                        if not barcode_brw:
                            lot_create = lot_obj.create({'name':i, 'product_id': brw_id_trans_det.product_id.id})
                            brw_id_trans_det.write({'quantity':1,'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':lot_create.id})
                        else:
                            brw_id_trans_det.write({'quantity':1,'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':barcode_brw.id.id})
                        counter = 0
                    else:
                        if not barcode_brw:
                            lot_create = lot_obj.create({'name':i, 'product_id': brw_id_trans_det.product_id.id})
                            trans_det_obj.create({'product_uom_id':brw_id_trans_det.product_id.uom_id.id, 'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id, 'lot_id':lot_create.id})
                        else:
                            trans_det_obj.create({'product_uom_id':brw_id_trans_det.product_id.uom_id.id, 'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id, 'lot_id':barcode_brw.id.id})
        elif (((len(data))-1) <= brw_id_trans_det.quantity):
            counter = 1
            if value[1] == 0:
                for i in data:
                    print "============i",i
                    if i!="":
                        barcode_brw = self.chk_barcode_availability(i)
                        if counter == 1:
                            if not barcode_brw:
                                lot_create = lot_obj.create({'name':i, 'product_id': brw_id_trans_det.product_id.id})
                                brw_id_trans_det.write({'quantity':value[0],'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':lot_create.id})
                            else:
                                brw_id_trans_det.write({'quantity':value[0],'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':barcode_brw.id.id})
                            counter = 0
                        else:
                            if not barcode_brw:
                                lot_create = lot_obj.create({'name':i, 'product_id': brw_id_trans_det.product_id.id})
                                res = trans_det_obj.create({'quantity':value[0],'product_uom_id':brw_id_trans_det.product_id.uom_id.id, 'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id, 'lot_id':lot_create.id})
                            else:
                                print "================barcode_brw",barcode_brw.id.id
                                res = trans_det_obj.create({'quantity':value[0],'product_uom_id':brw_id_trans_det.product_id.uom_id.id, 'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id, 'lot_id':barcode_brw.id.id})
#                 if not barcode_brw:
#                     lot_create = lot_obj.create({'name':data[0], 'product_id': brw_id_trans_det.product_id.id})
#                     brw_id_trans_det.write({'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':lot_create.id})
#                 else:
#                     brw_id_trans_det.write({'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':barcode_brw.id.id})
            else:
                counter = 1
                for i in data:
                    barcode_brw = self.chk_barcode_availability(i)
                    if i!="":
                        if counter == 1:
                            if not barcode_brw:
                                lot_create = lot_obj.create({'name':i, 'product_id': brw_id_trans_det.product_id.id})
                                brw_id_trans_det.write({'quantity':value[0],'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':lot_create.id})
                            else:
                                brw_id_trans_det.write({'quantity':value[0],'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id,'lot_id':barcode_brw.id.id})
                            counter = 0
                        elif not barcode_brw:
                            lot_create = lot_obj.create({'name':i, 'product_id': brw_id_trans_det.product_id.id})
                            res = trans_det_obj.create({'quantity':value[0],'product_uom_id':brw_id_trans_det.product_id.uom_id.id, 'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id, 'lot_id':lot_create.id})
                            create_value.append(res)
                            lot_ids.append(lot_create)
                        else:
                            print "================barcode_brw",barcode_brw.id.id
                            res = trans_det_obj.create({'quantity':value[0],'product_uom_id':brw_id_trans_det.product_id.uom_id.id, 'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id, 'lot_id':barcode_brw.id.id})
                            create_value.append(res)
                            lot_ids.append(barcode_brw.id)
                print "===========i",create_value
                for i in create_value:
                    print "===========i",i
                    if remainder != 0 :
                        i.write({'quantity':i.quantity+1,'product_id': brw_id_trans_det.product_id.id,'sourceloc_id':brw_id_trans_det.sourceloc_id.id,'product_uom_id':brw_id_trans_det.product_id.uom_id.id,'destinationloc_id':brw_id_trans_det.destinationloc_id.id,'transfer_id':brw_id_trans_det.transfer_id.id})
                        remainder= remainder-1
        else:
            raise Warning('Number of barcode is more then product quantity')
        res_id = brw_id_trans_det.transfer_id.id
#         brw_id_trans_det.unlink()
        dummy, view_id = self.env['ir.model.data'].get_object_reference('barcode', 'barode_product')
        return {
            'name':'return_barcode',
            'view_mode': 'form',
            'res_id': res_id,
            'view_type': 'form',
            'res_model': 'stock.transfer_details',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
            }
        }
        


class transfer_details_items(models.TransientModel):
    _inherit = "stock.transfer_details_items"
    
    @api.multi
    def barcode(self):
        dummy, view_id = self.env['ir.model.data'].get_object_reference('barcode', 'view_product_barcodes')

        return {
            'name':'Barcode',
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'product.barcodes',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
            }
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
